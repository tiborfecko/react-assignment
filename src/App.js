import React from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom";
import './App.css';

function App() {
    return (
      <BrowserRouter>
        <Route exact path='/' component={Search}/>
        <Route path='/overview/:employeeName' component={EmployeeRecord}/>
      </BrowserRouter>
    );
}

class Search extends React.Component{
constructor(props){
  super(props);
  this.state = {
    employee: ""
  };
}

render(){
  return(
    <div className="App">
    <h1>Employee explorer</h1>
     <input className="search-field" type="employeeName" value={this.state.employee} onChange={ event => this.setState({employee: event.target.value})}/>
     <Link className="button" to={`/overview/${this.state.employee}`}>Search</Link>
  </div>
  );
}
}

class EmployeeRecord extends React.Component {

constructor(props){
 super(props);

 this.subs = [];
 this.state = {
   subordinates: [],
   loadingFlag: true,
 }
 this.queryEmployeeSubordinates = this.queryEmployeeSubordinates.bind(this);
}

async queryEmployeeSubordinates(name)
{
  let response = await fetch('http://api.additivasia.io/api/v1/assignment/employees/' + name );
  let data = await response.json();
  if(data.length != 2 || !data[1].hasOwnProperty('direct-subordinates')){
    return;
  }

  let directSubordinates = data[1]['direct-subordinates'];
  for(let i=0; i< directSubordinates.length; i++){
    let subordinate = directSubordinates[i];
    if(!this.subs.includes(subordinate)){
      this.subs.push(subordinate);
     }  
    await this.queryEmployeeSubordinates(subordinate)  
  }
}

 async componentDidMount(){

    await this.queryEmployeeSubordinates(this.props.match.params.employeeName);
    this.setState({loadingFlag: false, subordinates: this.subs})
  }

  render(){
    let content = "";
    if(this.state.loadingFlag) {
      content = <LoadingEmployee employeeName={this.props.match.params.employeeName}/> 
    }else{
        content = <ShowingSubordinates employeeName={this.props.match.params.employeeName} subordinates = {this.state.subordinates} />            
    }
    
    return (
      <div className="App">
        <h1>Employee overview</h1>
        {content}       
      </div>
    );
  }
}

function ShowingSubordinates(props){
return (
  <div>
    <h2>Subordinates of employee {props.employeeName}: </h2>
    {props.subordinates.map(subordinate => <p><SubordinateRecord name={subordinate} /></p>)}
  </div>
);
}

function LoadingEmployee(props){
  return (<div>Loading overview of {props.employeeName} </div>);
}

function SubordinateRecord(props) {
    return( 
      <div>
      <p>{props.name}</p>
      </div>
    );
}

export default App;
